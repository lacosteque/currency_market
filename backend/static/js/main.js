loadCSS("/static/vendor/bootstrap-4.6.2-dist/css/bootstrap.min.css");
loadCSS("/static/vendor/gradient-free-theme/assets/css/LineIcons.2.0.css");
loadCSS("/static/vendor/gradient-free-theme/assets/css/animate.css");
loadCSS("/static/vendor/gradient-free-theme/assets/css/main.css");
loadCSS("/static/vendor/bootstrap-multiselect/dist/css/bootstrap-multiselect.min.css");
loadCSS("/static/vendor/bootstrap-table-master/dist/bootstrap-table.min.css");
loadCSS("/static/vendor/jquery-ui-1.13.2.custom/jquery-ui.min.css");
loadCSS("/static/vendor/jquery-ui-1.13.2.custom/jquery-ui.theme.min.css");



$(function () {
    if ($("#multiselect").length > 0) {
        (async () => {
            try {
                let response = await fetch("/v1/market/");
                if (response.ok) {
                    let data = await response.json();
                    let html = "";
                    for (const key in data) {
                        const value = data[key]["ID"];
                        const name = data[key]["Name"];
                        html += '<option value="' + value + '">' + name + "</option>";
                    }

                    document.querySelector("#multiselect").innerHTML = html;
                }
            } catch (error) {
                console.log(error);
            }

            $("#multiselect").multiselect({
                enableFiltering: true,
                includeFilterClearBtn: false,
                includeSelectAllOption: true,
                maxHeight: 350,
                numberDisplayed: 1,
            });
        })();
    }


    $("#datepicker").datepicker({ maxDate: "0", dateFormat: "dd/mm/yy" });

    var dateFormat = "dd/mm/yy",
        from = $("#from")
            .datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            }),
        to = $("#to")
            .datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }


    $("#get_daily_quote").click(function (e) {
        e.preventDefault();
        var $table = $("#table");

        var date = $("#datepicker").val();

        if ($(".multiselect-container .active").length > 0) {
            var selectednumbers = [];

            $(".multiselect-container .active input ").each(function (i, selected) {
                var currencyID = $(selected).val();

                selectednumbers.push(currencyID);
            });
            var data = { "currencyID": selectednumbers, "Date": date };

            var json = JSON.stringify(data);

            (async () => {
                try {
                    let response = await fetch("/v1/quotes/daily/", {
                        method: "POST",
                        body: json,
                    });
                    if (response.ok) {
                        let data = await response.json();
                        $table.bootstrapTable({ data: data, exportDataType: $(this).val(), exportTypes: ["csv", "excel", "pdf"] }).bootstrapTable("load", data);
                        var element = document.getElementById("table");
                        element.classList.remove("d-none");
                        $("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);
                    }
                } catch (error) {
                    console.log(error);
                }
            })();
        }
    });

    $("#get_dynamic_quote").click(function (e) {
        e.preventDefault();

        if ($(".multiselect-container .active").length > 0) {
            $("#myChart").remove();
            $("#result").append('<canvas id="myChart"></canvas>');

            var currencyID = $(".multiselect-container .active input ").val();
            var date_req1 = $("#from").val();
            var date_req2 = $("#to").val();
            var title = $(".custom-select").attr("title");

            var data = { "currencyID": currencyID, "date_req1": date_req1, "date_req2": date_req2 };

            var json = JSON.stringify(data);

            (async () => {
                try {
                    let response = await fetch("/v1/quotes/dynamic/", {
                        method: "POST",
                        body: json,
                    });
                    if (response.ok) {
                        let data = await response.json();

                        const ctx = document.getElementById("myChart");
                        const myChart = new Chart(ctx, {
                            type: "line",
                            data: {
                                datasets: [
                                    {
                                        label: title,
                                        data: data,
                                        backgroundColor: ["rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 206, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(255, 159, 64, 0.2)"],
                                        borderColor: ["rgba(255, 99, 132, 1)", "rgba(54, 162, 235, 1)", "rgba(255, 206, 86, 1)", "rgba(75, 192, 192, 1)", "rgba(153, 102, 255, 1)", "rgba(255, 159, 64, 1)"],
                                        borderWidth: 2,
                                    },
                                ],
                            },
                            options: {
                                spanGaps: true,

                                parsing: {
                                    xAxisKey: "Date",
                                    yAxisKey: "Value",
                                },
                            },
                        });


                        $("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);
                    }
                } catch (error) {
                    console.log(error);
                }
            })();
        }
    });
});
