from fastapi import APIRouter
from webapp.v1 import route_homepage
from webapp.v1 import route_daily_quotes
from webapp.v1 import route_dynamic_quotes

api_router = APIRouter()
api_router.include_router(route_homepage.router, prefix="", tags=["homepage"])
api_router.include_router(route_daily_quotes.router, prefix="", tags=["daily_quotes"])
api_router.include_router(route_dynamic_quotes.router, prefix="", tags=["dynamic_quotes"])









