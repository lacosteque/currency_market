from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
import uvicorn
from core.config import settings
from api.base import api_router
from webapp.base import api_router as web_router


def include_router(app):
    app.include_router(api_router)
    app.include_router(web_router)

def configure_static(app):
    app.mount("/static", StaticFiles(directory="static"), name="static")


def start_application():
    app = FastAPI(title=settings.PROJECT_NAME, version=settings.PROJECT_VERSION)
    include_router(app)
    configure_static(app)
    return app
 
app = start_application()

if __name__ == "__main__":
    uvicorn.run("__main__:app", host="0.0.0.0", port=8000, reload=True)
