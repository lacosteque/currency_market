from pydantic import BaseModel
from pydantic import validator
from datetime import datetime, date


class DailyQuotes(BaseModel):
    currencyID: list[str]
    Date: str


    @validator('Date', pre=True, always=True)
    def check_dayli_date_format(cls, value):
        datetime.strptime(value, "%d/%m/%Y")
        return value


class DynamicQuotes(BaseModel):
    currencyID: str
    date_req1: str
    date_req2: str

    
    @validator('date_req1', pre=True, always=True)
    def check_date_range1_format(cls, value):
        datetime.strptime(value, "%d/%m/%Y")
        return value

    
    @validator('date_req2', pre=True, always=True)
    def check_date_range2_format(cls, value):
        datetime.strptime(value, "%d/%m/%Y")
        return value




