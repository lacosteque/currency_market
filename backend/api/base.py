from fastapi import APIRouter
from api.v1 import route_market
from api.v1 import route_daily_quotes
from api.v1 import route_dynamic_quotes

api_router = APIRouter()
api_router.include_router(route_market.router, prefix="", tags=["market"])
api_router.include_router(route_daily_quotes.router, prefix="", tags=["daily_quotes"])
api_router.include_router(route_dynamic_quotes.router, prefix="", tags=["dynamic_quotes"])
