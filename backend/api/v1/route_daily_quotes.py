import httpx
from fastapi import APIRouter
from fastapi import Request
from fastapi import Depends
from fastapi.responses import ORJSONResponse
from pydantic import ValidationError, validator
from cbr.api import get_daily_quotes
from utils.http_request import get_client
from schemas.quotes import DailyQuotes


router = APIRouter(include_in_schema=False)


@router.post("/v1/quotes/daily/", response_class=ORJSONResponse)
async def daily_quotes(request: Request, client = Depends(get_client)):
    data = await request.json()
    try:
        dataQuotes = DailyQuotes(**data)
        output = await get_daily_quotes(client, dataQuotes)
        return output

    except ValidationError as e:
        return e.json()
    
