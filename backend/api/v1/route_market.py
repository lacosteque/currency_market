import httpx
from fastapi import Request
from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import ORJSONResponse
from cbr.api import get_list_currencies
from utils.http_request import get_client


router = APIRouter(include_in_schema=False)


@router.get("/v1/market/", response_class=ORJSONResponse)
async def market(request: Request, client = Depends(get_client)):
        output = await get_list_currencies(client)
        return output
