import httpx

import xml.etree.ElementTree as ET
from typing import Iterable
from utils.http_request import request as http
from schemas.quotes import DailyQuotes
from schemas.quotes import DynamicQuotes


async def _get_xml(client: httpx.AsyncClient, url: str, params: dict = None) -> ET.Element:
    xml = await http(client, url, params)
    output = ET.fromstring(xml)
    return output


async def get_list_currencies(client: httpx.AsyncClient) -> list:
    url = 'https://www.cbr.ru/scripts/XML_val.asp'
    xml = await _get_xml(client, url)
    return [ {
              'Name': currency.find('EngName').text,
              'ID': currency.get('ID'),
             } for currency in xml.findall('Item') ]


async def get_daily_quotes(client: httpx.AsyncClient, data: DailyQuotes) -> list:
    url = f'https://www.cbr.ru/scripts/XML_daily.asp?date_req={data.Date}'
    xml = await _get_xml(client, url)
    return [  
    {
    'CharCode': currency.find('CharCode').text,
    'Nominal': currency.find('Nominal').text,
    'Name':currency.find('Name').text,
    'Value':currency.find('Value').text,
    'Date': xml.get('Date'),
        } for Valute_ID in data.currencyID for currency in xml.findall('Valute') if currency.get('ID') == Valute_ID ]


async def get_dynamic_quotes(client: httpx.AsyncClient, data: DynamicQuotes) -> list:
    params = {'date_req1': data.date_req1,
              'date_req2': data.date_req2,
              'VAL_NM_RQ': data.currencyID,
              }
    url = 'https://www.cbr.ru/scripts/XML_dynamic.asp'
    xml = await _get_xml(client, url, params)
    return [ { 'Date' : currency.get('Date'), 
               'Value': float(currency.find('Value').text.replace(',','.')) } for currency in xml.findall('Record') ]
   


   
